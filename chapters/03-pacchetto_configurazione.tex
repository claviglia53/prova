\chapter{Pacchetto di configurazione} 
\label{chapter:configuration_packet}

Per permettere ad ArgoView di configurare ArgoBox (vedi punto \ref{item:2}),
ArgoMessage definisce il pacchetto di \emph{configurazione}.

Il pacchetto di configurazione ha come mittente ArgoView e come destinatario
ArgoBox ed è identificato dai bit \texttt{[7:6]} del primo byte di un generico
ArgoMessage settati a \texttt{b01}.

Pacchetti di configurazione in genere comportano un operazione di salvataggio
da parte di ArgoBox nella sua memoria non-volatile, o in generale
causano una variazione del comportamento di ArgoBox su lungo termine.

In particolare con un pacchetto di configurazione ArgoView può:

\begin{itemize}
	\item configurare i parametri di comunicazione di ArgoBox con una periferica;
	\item definire la strategia di gestione degli eventi di ArgoBox.
\end{itemize}

A ogni corretta applicazione e/o salvataggio della configurazione imposta
da ArgoView, ArgoBox risponde con un ArgoMessage di risposta di tipo
ACK (vedi \ref{subsection:response_packet:ack}).

In caso di errore (per esempio errore di scrittura sulla EEPROM per il salvataggio
della configurazione), ArgoBox risponde ad ArgoView con un ArgoMessage di risposta
di tipo diagnostica (vedi \ref{subsection:response_packet:diagnostic}).

\section{Configurazione della gestione degli eventi: eventi ed azioni}

Il protocollo definisce una serie di eventi (vedi appendice \ref{appendix:eventi})
identificati da un id progressivo \texttt{[0,255]}.
Questi vengono classificati in due macro-categorie: eventi generici ed \emph{errori}.

La configurazione per la gestione di \emph{errori} ha effetto solo qualora
ArgoBox non riuscisse a collegarsi ad ArgoView, altrimenti ArgoBox
si limita a riportare la condizione di errore ad ArgoView.

Per ogni evento ArgoView può definire un elenco di \emph{azioni} da eseguire.
Anche queste sono identificate da un id progressivo \texttt{[0,255]},
dove gli id appartenenti a \texttt{[0,64]} sono riservati ad azioni
predefinite (vedi appendice \ref{appendix:azioni}), mentre i restanti id sono
personalizzabili.

Fanno parte di azioni predefinite tutte le azioni di cui ArgoBox come
sistema è semanticamente consapevole.

Fanno parte di azioni personalizzabili tutte le azioni di cui ArgoBox non
conosce la semantica, ma si limita ad eseguire in base alla programmazione
di ArgoView.

Fatta questa distinzione, è dunque immediato che il modo più semplice
di descrivere un'azione personalizzabile è quello di riusare il pacchetto
di comando.

Anche la personalizzazione di un azione avviene tramite pacchetto
di configurazione.

\section{Formato del pacchetto}

\label{chapter:configuration_packet:packet_format}

\begin{figure}[h!]
	\centering
	\begin{bytefield}[bitwidth={.75\textwidth/8},endianness=big]{8}
		\bitheader{0-7} \\
		\begin{rightwordgroup}{1\degree byte}
			\bitbox{2}{\texttt{b01}} & \bitbox{6}{Tipo di configurazione}
		\end{rightwordgroup}
	\end{bytefield}
	\caption{Pacchetto di configurazione - parte comune}
\end{figure}

Il primo byte di un pacchetto di configurazione, oltre ai due bit
usati per l'identificazione del pacchetto stesso, comprende un campo
\emph{Tipo di configurazione} (\texttt{[5:0]}) in base alla quale la restante porzione
verrà interpretata.

Questo campo può assumere i seguenti valori:

\begin{enumerate}
	\item Configurazione periferica (\texttt{[0x01,0x06]}).
	\item Configura un'azione (\texttt{0x07}).
	\item Configura gestione di un evento (\texttt{0x08}).
\end{enumerate}

\subsection{Configurazione periferica}

\begin{figure}[h!]
	\centering
	\begin{bytefield}[bitwidth={.75\textwidth/8},endianness=big]{8}
		\begin{leftwordgroup}{20 bytes}
			\bitheader{0-7} \\
			\begin{rightwordgroup}{1\degree byte}
				\bitbox{2}{\texttt{b01}} & \bitbox{6}{ID periferica (\texttt{[0x01, 0x06]})}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{19 bytes}
				\wordbox[lrt]{1}{Configurazione periferica} \\
				\skippedwords \\
				\wordbox[lrb]{1}{}
			\end{rightwordgroup}
		\end{leftwordgroup}
	\end{bytefield}
	\caption{Pacchetto di configurazione - configurazione periferica}
\end{figure}

Dove: 
\begin{itemize}
	\item \emph{ID periferica}: rappresenta l'ID della periferica di cui si
		intende configurare i parametri di comunicazione.
\end{itemize}

Vedi appendice \ref{appendix:periferiche:configurazione_periferiche} per
dettagli sul payload di configurazione per ognuna delle sei periferiche.

\subsection{Configurazione di un'azione personalizzabile}

\begin{figure}[h!]
	\centering
	\begin{bytefield}[bitwidth={.75\textwidth/8},endianness=big]{8}
		\begin{leftwordgroup}{20 bytes}
			\bitheader{0-7} \\
			\begin{rightwordgroup}{1\degree byte}
				\bitbox{2}{\texttt{b01}} & \bitbox{6}{\texttt{0x07}}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{2\degree byte}
				\wordbox{1}{ID azione}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{18 bytes}
				\wordbox[lrt]{1}{Pacchetto di comando (senza gli ultimi due byte)} \\
				\skippedwords \\
				\wordbox[lrb]{1}{}
			\end{rightwordgroup}
		\end{leftwordgroup}
	\end{bytefield}
	\caption{Pacchetto di configurazione - configurazione di un'azione}
\end{figure}

Dove:

\begin{itemize}
	\item \emph{ID azione} ($\in [65, 255]$): identica l'azione che si vuole
		personalizzazare.
	\item \emph{pacchetto di comando}: rappresenta il comando associato
		all'azione.

		Quest'ultimo in questo caso non verrà eseguito immediatamente da
		ArgoBox, ma salvato nella sua memoria non-volatile.

		Nota che per racchiudere il pacchetto di comando è stato
		necessario togliere gli ultimi due byte del pacchetto di comando.
		O meglio, è proprio per necessità di racchiudere un pacchetto
		di comando in uno di configurazione che si è deciso di stabilire
		la lunghezza effettiva (senza dummy bytes) del pacchetto di comando
		a 18 bytes.
\end{itemize}

\newpage

\subsection{Configurazione di un evento}

\begin{figure}[h!]
	\centering
	\begin{bytefield}[bitwidth={.75\textwidth/8},endianness=big]{8}
		\begin{leftwordgroup}{20 bytes}
			\bitheader{0-7} \\
			\begin{rightwordgroup}{1\degree byte}
				\bitbox{2}{\texttt{b01}} & \bitbox{6}{\texttt{0x08}}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{2\degree byte}
				\wordbox{1}{ID evento}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{18 bytes}
				\wordbox{1}{ID 1 \degree azione} \\
				\wordbox{1}{ID 2 \degree azione} \\
				\wordbox[]{1}{$\vdots$} \\[1ex]
				\wordbox{1}{ID 18\degree azione}
			\end{rightwordgroup}
		\end{leftwordgroup}
	\end{bytefield}
	\caption{Pacchetto di configurazione - configurazione di un evento}
\end{figure}

Dove:

\begin{itemize}
	\item \emph{ID evento}: identifica l'evento di cui si intende impostare
		la strategia di error handling.
	\item 1\ldots18-esimo ID azioni ($\in [0, 255]$): rappresentano le azioni,
		predefinite o personalizzate, che verranno eseguite sequenzialmente al
		manifestarsi dell'evento identificato da \emph{ID evento}.
\end{itemize}

\section{Sequenza temporale}

\begin{figure}[!h]
	\centering
	\includegraphics[width={0.5\textwidth}]{assets/pacchetto_di_configurazione/best_case}
	\caption{Pacchetto di configurazione: best case}
\end{figure}

\begin{figure}[!h]
	\centering
	\includegraphics[width={0.5\textwidth}]{assets/pacchetto_di_configurazione/errore}
	\caption{Pacchetto di configurazione: errore}
\end{figure}
