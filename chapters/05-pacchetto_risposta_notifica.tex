\chapter{Pacchetto di risposta o notifica}

Tutti i tipi di pacchetto sopraelencati prevedono una risposta da ArgoBox,
la quale può essere un semplice ACK, ma anche un insieme di dati relativamente
complessi e strutturati.
Inoltre il protocollo deve permettere ad ArgoBox di notificare ad ArgoView
eventuali stati di errori (vedi punto \ref{item:4}).


ArgoMessage definisce dunque il pacchetto di \emph{risposta o notifica},
che ha come mittente ArgoBox e come destinatario ArgoView
ed è identificato dai bit \texttt{[7:6]} del primo byte di un generico
ArgoMessage settati a \texttt{b11}.

\section{Formato del pacchetto}

\begin{figure}[h!]
	\centering
	\begin{bytefield}[bitwidth={.75\textwidth/8},endianness=big]{8}
		\bitheader{0-7} \\
		\begin{rightwordgroup}{1\degree byte}
			\bitbox{2}{\texttt{b11}} &
			\bitbox{2}{\tiny{Tipo di risposta/notifica}} &
			\bitbox{4}{$\hdots$}
		\end{rightwordgroup}
	\end{bytefield}
	\caption{Pacchetto di risposta - parte comune}
\end{figure}

L'interpretazione del pacchetto di stato varia in base al valore del campo
\emph{Tipo di risposta/notifica}.

Questo campo può assumere i seguenti valori:

\begin{enumerate}
	\item ACK (\texttt{b00}):
	\item Diagnostica (\texttt{b01}):
	\item Configurazione (\texttt{b10}):
	\item Dato (\texttt{b11}):
\end{enumerate}

\subsection{Risposta di tipo ACK (\texttt{b00})}
\label{subsection:response_packet:ack}

Una risposta di tipo ACK non contiene nessun dato e viene usato solo per
segnalare ad ArgoView: 

\begin{itemize}
	\item il successo di una comunicazione con una periferica, che però non prevede
		nessun messaggio di ritorno;
	\item la corretta applicazione e/o il corretto salvataggio della configurazione.
\end{itemize}

\begin{figure}[h!]
	\centering
	\begin{bytefield}[bitwidth={.75\textwidth/8},endianness=big]{8}
		\bitheader{0-7} \\
		\begin{rightwordgroup}{1\degree byte}
			\bitbox{2}{\texttt{b11}} &
			\bitbox{2}{\texttt{b00}}
			\bitbox{4}{\texttt{b0000}}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{2\degree byte}
			\wordbox{1}{\texttt{0xAA}}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{18 bytes}
			\wordbox{1}{\texttt{0x00}} \\
			\wordbox[]{1}{$\vdots$} \\[1ex]
			\wordbox{1}{\texttt{0x00}}
		\end{rightwordgroup}
	\end{bytefield}
	\caption{Pacchetto di risposta - ACK}
\end{figure}

\newpage

\subsection{Risposta/notifica di tipo diagnostica (\texttt{b01})}
\label{subsection:response_packet:diagnostic}

Un pacchetto di risposta di tipo diagnostica viene mandato nei seguenti casi:

\begin{itemize}
	\item su richiesta esplicita di ArgoView tramite un ArgoMessage
		di stato di tipo diagnostica;
	\item quando la comunicazione con una periferica, a seguito ad un
		ArgoMessage di tipo comando, entra in timeout o in errore;
	\item quando ArgoBox fallisce l'applicazione e/o il salvataggio della
		configurazione mandata da ArgoView con un ArgoMessage di configurazione;
	\item quando ArgoBox rileva un errore e lo notifica spontaneamente ad ArgoView.
\end{itemize}

Nel pacchetto di diagnostica viene mandato sempre \emph{l'intero} stato di ArgoBox.

\begin{figure}[h!]
	\centering
	\begin{bytefield}[bitwidth={.75\textwidth/8},endianness=big]{8}
		\begin{leftwordgroup}{20 bytes}
			\bitheader{0-7} \\
			\begin{rightwordgroup}{1\degree byte}
				\bitbox{2}{\texttt{b11}} &
				\bitbox{2}{\texttt{b01}}
				\bitbox{1}{{errore}}
				\bitbox{3}{ID periferica}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{2\degree byte}
				\wordbox{1}{Misura alimentazione 12V}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{3\degree byte}
				\wordbox{1}{Misura alimentazione 5V}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{4\degree byte}
				\wordbox{1}{Misura alimentazione 3.3V}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{5\degree byte}
				\wordbox{1}{Misura tensione caricabatterie}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{6\degree byte}
				\wordbox{1}{Misura temperatura}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{7\degree byte}
				\wordbox{1}{Stato CAN}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{8\degree byte}
				\wordbox{1}{Stato RS485}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{9\degree byte}
				\wordbox{1}{Stato RS232 principale}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{10\degree byte}
				\wordbox{1}{Stato RS232 1}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{11\degree byte}
				\wordbox{1}{Stato RS232 2}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{12\degree byte}
				\wordbox{1}{Stato RS232 3}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{13\degree byte}
				\wordbox{1}{Stato EEPROM}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{14\degree byte}
				\wordbox{1}{Stato STM32}
			\end{rightwordgroup} \\
			\begin{rightwordgroup}{restanti 6 bytes}
				\wordbox{1}{\texttt{0x00}} \\
				\wordbox[]{1}{$\vdots$} \\[1ex]
				\wordbox{1}{\texttt{0x00}}
			\end{rightwordgroup}
		\end{leftwordgroup}
	\end{bytefield}
	\caption{Pacchetto di risposta/notifica - diagnostica}
\end{figure}

Dove:

\begin{itemize}
	\item \emph{errore} e \emph{ID periferica}: questi due campi disambiguano le
		tre condizioni in cui questo tipo di risposta o notifica viene mandato
		ad ArgoView. Ma anche senza questi dati di supporto, ArgoView dovrebbe
		essere in grado di discernere i tre casi in base al contesto della comunicazione
		con ArgoBox, che è stateful.

		In particolare:
		\begin{itemize}
			\item il bit \emph{errore} settato a 0 implica che la
				diagnostica è stata trasmessa su richiesta esplicita di
				ArgoView mediante un ArgoMessage di tipo stato.
			\item il bit \emph{errore} settato a 1 e il campo
				\emph{ID periferica} settato a 0 implica che l'errore non
				riguarda la comunicazione con le periferiche. È dunque
				ArgoBox che, rilevando lo stato d'errore, decide spontaneamente
				di notificare ArgoView.
			\item il bit \emph{errore} settato a 1 e il campo 
				\emph{ID periferica} settato ad un valore appartenente
				al dominio dell'ID periferica implica che l'errore
				riguarda la comunicazione con la periferica identificata
				da \emph{ID periferica}.
		\end{itemize}
	\item Per il formato degli altri campi, vedi appendice 
		\ref{appendix:formato_diagnostica}.
\end{itemize}

\subsection{Risposta di tipo configurazione (\texttt{b10})}

Un pacchetto di risposta di tipo configurazione viene mandato nel seguente caso:

\begin{itemize}
	\item su richiesta esplicita di ArgoView tramite un ArgoMessage
		di stato di tipo diagnostica.
\end{itemize}

\begin{figure}[!h]
	\centering
	\begin{bytefield}[bitwidth={.75\textwidth/8},endianness=big]{8}
		\bitheader{0-7} \\
		\begin{rightwordgroup}{1\degree byte}
			\bitbox{2}{\texttt{b01}} &
			\bitbox{2}{\texttt{b10}}
			\bitbox{4}{\texttt{b0000}}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{19 bytes}
			\wordbox[lrt]{1}{Configurazione richiesta} \\
			\skippedwords \\
			\wordbox[lrb]{1}{}
		\end{rightwordgroup}
	\end{bytefield}
	\caption{Pacchetto di risposta - configurazione}
\end{figure}

Nota che il pacchetto non contiene informazioni che identificano
la configurazione. Infatti, un pacchetto di risposta di tipo configurazione
viene mandato solo su richiesta esplicita di ArgoView, mediante un pacchetto
di stato di tipo configurazione. Per cui ArgoView conosce la configurazione
in contesto.

\subsection{Risposta di tipo dato (\texttt{b11})}
\label{subsection:response_packet:data}

Un pacchetto di risposta di tipo dato viene mandato nel seguente caso:

\begin{itemize}
	\item dopo una corretta comunicazione con una periferica da parte di
		ArgoBox, a seguito di un ArgoMessage di comando da ArgoView.
		Inoltre, il valore del campo \emph{Numero di byte da aspettarsi dalla
		periferica} dell'ArgoMessage di comando deve essere diverso
		da 0.
\end{itemize}

\begin{figure}[!h]
	\centering
	\begin{bytefield}[bitwidth={.75\textwidth/8},endianness=big]{8}
		\bitheader{0-7} \\
		\begin{rightwordgroup}{1\degree byte}
			\bitbox{2}{\texttt{b11}} &
			\bitbox{2}{\texttt{b11}}
			\bitbox{3}{contatore multi-messaggio}
			\bitbox{1}{\texttt{b0}}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{2\degree byte}
			\wordbox{1}{Lunghezza messaggio}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{18 bytes}
			\wordbox[lrt]{1}{Messaggio (risposta da una periferica da inoltrare
			ad ArgoView)} \\
			\skippedwords \\
			\wordbox[lrb]{1}{}
		\end{rightwordgroup}
	\end{bytefield}
	\caption{Pacchetto di risposta - dati}
\end{figure}

Dove: 

\begin{itemize}
	\item \emph{Lunghezza messaggio}: rappresenta il numero di bytes
		di cui è composto il messaggio. Se il suo valore fosse \textgreater 18,
		si attiva il meccanismo di packet segmentation.
		
		Vedi campo \emph{Contatore multi-messaggio}.
	\item \emph{Contatore multi-messaggio}: analogamente all'ArgoMessage
		di comando a cui questo pacchetto risponde, è possibile
		che i 18 bytes del campo \emph{Messaggio} non siamo sufficienti
		per trasmettere l'intero messaggio della periferica.
		Questo campo assume dunque valore 0, quando il campo \emph{Lunghezza messaggio}
		è $\leq 18$, altrimenti identifica l'n-esimo pacchetto trasmesso,
		in maniera analoga al campo \emph{Contatore multi-messaggio} 
		del pacchetto di comando (vedi \ref{chapter:command_packet:multimessage-counter}).
\end{itemize}
